#!/bin/bash

# We copy the scripts to every apex instances
for I in 1 2 3 4 5 6
do
   for P in torso ergo
   do
     for F in deploy.sh update.sh activate.sh
     do
        echo "Copying $F to apex-$I-$P.local"
        scp ../raspberrypi/$F pi@apex-$I-$P.local:/home/pi/$F || { echo "Failed to copy $F to apex-$I-$P.local"; exit 1; }
        ssh pi@apex-$I-$P.local "chmod +x $F" || { echo "Failed to chmod $F on apex-$I-$P.local"; exit 2; }
      done
   done
done

echo "Installation Succeded ..."