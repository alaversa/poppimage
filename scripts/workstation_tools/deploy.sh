#!/bin/bash

# We retrieve the repository from command line arguments
REPOSITORY=$1
REGEX="(.*)/([^/]*)(\.git)"

# We retrieve the name of the repo
if [[ $REPOSITORY =~ $REGEX ]]; then
   FOLDER_NAME="${BASH_REMATCH[2]}_ws"
   REPO_NAME="${BASH_REMATCH[2]}"
fi

# We install dependencies
sudo apt install python-pip
sudo pip install inputs flask flask_cors explauto termcolor

# We create a workspace
echo "Creating a workspace in $FOLDER_NAME"
mkdir -p /home/flowers/$FOLDER_NAME/src || { echo "Failed to make workspace directory"; exit 1; }
cd /home/flowers/$FOLDER_NAME/src

# We clone the repositories
echo "Cloning repositories ..."
git clone $REPOSITORY ||{ echo "Failed to clone $REPOSITORY"; exit 2; }
git clone https://github.com/ymollard/poppy_torso_controllers.git || { echo "Failed to clone poppy_torso_controllers"; exit 3; }
git clone https://github.com/ymollard/poppy_ergo_jr_controllers.git || { echo "Failed to clone poppy_ergo_jr_controlllers"; exit 4;}
git clone https://github.com/ymollard/poppy_msgs.git || { echo "Failed to clone poppy_msgs."; exit 5; }

# We compile
echo "Compiling Package ..."
cd /home/flowers/$FOLDER_NAME
catkin_make || { echo "Failed to compile ros package"; exit 6; }

# Install autostart if compiling successed
bash /home/flowers/$FOLDER_NAME/src/$REPO_NAME/scripts/apex.sh || { echo "Failed to execute apex.sh "; exit 7; }

# We deploy to every apex instances
for I in 1 2 3 4 5 6
do
   for P in torso ergo
   do
      echo "Deploying repository on apex-$I-$P.local"
      ssh pi@apex-$I-$P.local "sudo ./deploy.sh $REPOSITORY" || { echo "Failed to deploy on apex-$I-$P.local"; exit 8; }
    done
done

echo "Deployment of $REPOSITORY succeeded ..."

