#!/bin/bash

# We retrieve the repository from command line arguments
REPOSITORY=$1
REGEX="(.*)/([^/]*)(\.git)"

# We retrieve the name of the repo
if [[ $REPOSITORY =~ $REGEX ]]; then
   FOLDER_NAME="${BASH_REMATCH[2]}_ws"
   REPO_NAME="${BASH_REMATCH[2]}"
fi

# We clear ros
echo "Killing rosnodes"
rosnode kill -a
pkill roscore

# We activate the new env on apex-controller
cd /home/flowers/$FOLDER_NAME
bash /home/flowers/$FOLDER_NAME/src/$REPO_NAME/scripts/apex.sh || { echo "Failed to start apex.sh"; exit 8; }

# We activate on  every apex instances
for I in 1 2 3 4 5 6
do
   for P in torso ergo
   do
      echo "Updating repository on apex-$I-$P.local"
      ssh pi@apex-$I-$P.local "sudo ./update.sh $REPOSITORY" || { echo "Failed to update on apex-$I-$P.local"; exit 9; }
    done
done

echo "Update of $REPOSITORY succeeded ..."