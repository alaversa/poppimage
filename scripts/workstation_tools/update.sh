#!/bin/bash

# We retrieve the repository from command line arguments
REPOSITORY=$1
REGEX="(.*)/([^/]*)(\.git)"

# We retrieve the name of the repo
if [[ $REPOSITORY =~ $REGEX ]]; then
   FOLDER_NAME="${BASH_REMATCH[2]}_ws"
   REPO_NAME="${BASH_REMATCH[2]}"
fi

# We pull the repository
echo "Pulling $REPOSITORY"
cd /home/flowers/$FOLDER_NAME/src
git pull || { echo "Failed to pull"; exit 1; }

# We compile
echo "Compiling Package ..."
cd /home/flowers/$FOLDER_NAME
catkin_make || { echo "Failed to compule ros package"; exit 2; }

# Install autostart if compiling successed
bash /home/flowers/$FOLDER_NAME/src/$REPO_NAME/scripts/apex.sh || { echo "Failed to execute apex.sh"; exit 3; }

# We deploy to every apex instances
for I in 1 2 3 4 5 6
do
   for P in torso ergo
   do
      echo "Updating repository on apex-$I-$P.local"
      ssh pi@apex-$I-$P.local "sudo ./update.sh $REPOSITORY" || { echo "Failed to update on apex-$I-$P.local"; exit 4; }
    done
done

echo "Update of $REPOSITORY succeeded ..."


