#!/bin/bash

# We retrieve the repository from command line arguments
REPOSITORY=$1
REGEX="(.*)/([^/]*)(\.git)"

# We retrieve the name of the repo
if [[ $REPOSITORY =~ $REGEX ]]; then
   FOLDER_NAME="${BASH_REMATCH[2]}_ws"
   REPO_NAME="${BASH_REMATCH[2]}"
fi

# We check that the repo is here.
if [[ ! -d "/home/pi/$FOLDER_NAME" ]]; then
   echo "Repository doesn't exists. Try to deploy first."
   exit 1
fi

# We clone the repositories
echo "Pulling repository ..."
cd /home/pi/$FOLDER_NAME/src/REPO_NAME
git pull || { echo "Failed to pull repository "; exit 1; }

# We compile
echo "Compiling Package ..."
cd /home/pi/$FOLDER_NAME
catkin_make || { echo "Failed to build ros package"; exit 2; }

# Install autostart if compiling successed
source /home/pi/setup.bash || { echo "Failed to source the setup.bash"; exit 3; }
sudo systemctl daemon-reload || { echo "Failed to reload apex service"; exit 4; }
sudo systemctl enable apex.service || { echo "Failed to enable apex service"; exit 5; }