#!/bin/bash

# We retrieve the repository from command line arguments
REPOSITORY=$1
REGEX="(.*)/([^/]*)(\.git)"

# We retrieve the name of the repo
if [[ $REPOSITORY =~ $REGEX ]]; then
   FOLDER_NAME="${BASH_REMATCH[2]}_ws"
   REPO_NAME="${BASH_REMATCH[2]}"
fi

# We check that the repo is here.
if [[ ! -d "/home/pi/$FOLDER_NAME" ]]; then
   echo "Repository doesn't exists. Try to deploy first."
   exit 1
fi

# We activate the workspace
echo "Activating workspace"
rosnode kill -a 
source /home/pi/$OLDER_NAME/devel/setup.bash || { echo "Failed to source setup"; exit 1; }
sudo cp apex.service /lib/systemd/system || { echo "Failed to copy to apex service"; exit 1; }
ln -s /home/pi/$FOLDER_NAME/src/$REPO_NAME/scripts/raspberrypi/autostart.bash ./autostart.bash || { echo "Failed to symlink autostart"; exit 1; }
ln -s /home/pi/$FOLDER_NAME/devel/setup.bash ./setup.bash || { echo "Failed to symlink setup"; exit 1; }
sudo systemctl daemon-reload || { echo "Failed to reload dameon"; exit 1; }
sudo systemctl enable apex.service || { echo "Failed to enable apex service"; exit 1; }