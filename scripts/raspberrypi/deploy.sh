#!/bin/bash

# We retrieve the repository from command line arguments
REPOSITORY=$1
REGEX="(.*)/([^/]*)(\.git)"

# We retrieve the name of the repo
if [[ $REPOSITORY =~ $REGEX ]]; then
   FOLDER_NAME="${BASH_REMATCH[2]}_ws"
   REPO_NAME="${BASH_REMATCH[2]}"
fi

# We create a workspace
echo "Creating a workspace in $FOLDER_NAME"
mkdir -p /home/pi/$FOLDER_NAME/src || { echo "Failed to make workspace dir."; exit 1; }
cd /home/pi/$FOLDER_NAME/src

# We clone the repositories
echo "Cloning repositories ..."
git clone $REPOSITORY || { echo "Failed to clone $REPOSITORY"; exit 2; }
git clone https://github.com/ymollard/poppy_torso_controllers.git || { echo "Failed to clone poppy_torso_controllers"; exit 2; }
git clone https://github.com/ymollard/poppy_ergo_jr_controllers.git || { echo "Failed to clone poppy_ergo_jr_controllers"; exit 2; }
git clone https://github.com/ymollard/poppy_msgs.git || { echo "Failed to clone poppy_msgs"; exit 2; }

# We compile
echo "Compiling Package ..."
cd /home/pi/$FOLDER_NAME
catkin_make || { echo "Failed to build ros package"; exit 3; }

# Install autostart if compiling successed
ln -s /home/pi/$FOLDER_NAME/devel/setup.bash ./setup.bash || { echo "Failed to symlink setup.bash", }
source /home/pi/setup.bash || { echo "Failed to source the setup.bash"; exit 4; }
ln -s /home/pi/$FOLDER_NAME/src/$REPO_NAME/scripts/raspberrypi/autostart.bash ./autostart.bash || { echo "Failed to symink autostart"; exit 6; }
sudo cp apex.service /lib/systemd/system || { echo "Failed to copy the apex service "; exit 5; }
sudo systemctl daemon-reload || { echo "Failed to reload apex service"; exit 6; }
sudo systemctl enable apex.service || { echo "Failed to enable apex service "; exit 7; }